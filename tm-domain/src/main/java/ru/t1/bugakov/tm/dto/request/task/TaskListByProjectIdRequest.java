package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable String projectId) {
        this.projectId = projectId;
    }

    public TaskListByProjectIdRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
