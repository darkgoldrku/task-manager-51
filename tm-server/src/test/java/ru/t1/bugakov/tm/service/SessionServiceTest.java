package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.api.service.dto.ISessionDTOService;
import ru.t1.bugakov.tm.api.service.dto.ITaskDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.SessionDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.marker.DatabaseCategory;
import ru.t1.bugakov.tm.marker.UnitCategory;
import ru.t1.bugakov.tm.service.dto.ProjectDTOService;
import ru.t1.bugakov.tm.service.dto.SessionDTOService;
import ru.t1.bugakov.tm.service.dto.TaskDTOService;
import ru.t1.bugakov.tm.service.dto.UserDTOService;

import java.util.List;
import java.util.UUID;

@Category(DatabaseCategory.class)
public final class SessionServiceTest {

    @NotNull
    private static final String USER_TEST_LOGIN = "admin";

    @NotNull
    private static final String USER_TEST_PASSWORD = "1234";

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE, PROPERTY_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static final String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    private static final SessionDTO TEST_SESSION_1 = new SessionDTO();

    @NotNull
    private static final SessionDTO TEST_SESSION_2 = new SessionDTO();

    @NotNull
    private final ISessionDTOService service = new SessionDTOService(CONNECTION_SERVICE);

    @BeforeClass
    public static void setUp() throws Exception {
        USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void before() throws Exception {
        service.add(USER_ID, TEST_SESSION_1);
        service.add(USER_ID, TEST_SESSION_2);
    }

    @After
    public void after() {
        service.clear(USER_ID);
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final SessionDTO TEST_Session_3 = new SessionDTO();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, TEST_Session_3));
        service.add(USER_ID, TEST_Session_3);
        @Nullable final SessionDTO session = service.findById(USER_ID, TEST_Session_3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(TEST_Session_3.getId(), session.getId());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        final List<SessionDTO> sessions = service.findAll(USER_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(USER_ID, session.getUserId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertFalse(service.existsById(USER_ID, ""));
        Assert.assertFalse(service.existsById(USER_ID, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_ID, TEST_SESSION_1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", TEST_SESSION_1.getId()));
        Assert.assertNull(service.findById(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = service.findById(USER_ID, TEST_SESSION_1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(TEST_SESSION_1.getId(), session.getId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(""));
        service.clear(USER_ID);
        Assert.assertEquals(0, service.getSize(USER_ID));
    }

    @Test
    public void removeByUserId() {
        service.remove(USER_ID, TEST_SESSION_2);
        Assert.assertNull(service.findById(USER_ID, TEST_SESSION_2.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER_ID, ""));
        service.removeById(USER_ID, TEST_SESSION_2.getId());
        Assert.assertNull(service.findById(USER_ID, TEST_SESSION_2.getId()));
    }

    @Test
    public void getSizeByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(2, service.getSize(USER_ID));
    }

}

