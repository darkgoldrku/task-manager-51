package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.api.service.dto.ITaskDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.marker.DatabaseCategory;
import ru.t1.bugakov.tm.marker.UnitCategory;
import ru.t1.bugakov.tm.service.dto.ProjectDTOService;
import ru.t1.bugakov.tm.service.dto.TaskDTOService;
import ru.t1.bugakov.tm.service.dto.UserDTOService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Category(DatabaseCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final String USER_TEST_LOGIN = "admin";

    @NotNull
    private static final String USER_TEST_PASSWORD = "1234";

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectDTOService PROJECT_SERVICE = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDTOService SERVICE = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserDTOService USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, PROJECT_SERVICE, SERVICE, PROPERTY_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static final String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

    @NotNull
    private static final ProjectDTO TEST_PROJECT_1 = new ProjectDTO();

    @NotNull
    private static final ProjectDTO TEST_PROJECT_2 = new ProjectDTO();

    @NotNull
    private static final TaskDTO TEST_TASK_1 = new TaskDTO();

    @NotNull
    private static final TaskDTO TEST_TASK_2 = new TaskDTO();

    @NotNull
    private static final TaskDTO TEST_TASK_3 = new TaskDTO();

    @BeforeClass
    public static void setUp() throws Exception {
        USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void before() throws Exception {
        TEST_PROJECT_1.setName("Test Project 1");
        TEST_PROJECT_1.setDescription("Test Description 1");
        TEST_PROJECT_2.setName("Test Project 2");
        TEST_PROJECT_2.setDescription("Test Description 2");
        PROJECT_SERVICE.add(USER_ID, TEST_PROJECT_1);
        PROJECT_SERVICE.add(USER_ID, TEST_PROJECT_2);
        TEST_TASK_1.setName("Test Task 1");
        TEST_TASK_1.setDescription("Test Description 1");
        TEST_TASK_2.setName("Test Task 2");
        TEST_TASK_2.setDescription("Test Description 2");
        TEST_TASK_3.setName("Test Task 3");
        TEST_TASK_3.setDescription("Test Description 3");
        SERVICE.add(USER_ID, TEST_TASK_1);
        SERVICE.add(USER_ID, TEST_TASK_2);
    }

    @After
    public void after() {
        SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.add("", TEST_TASK_3));
        @Nullable final TaskDTO task = SERVICE.findById(USER_ID, TEST_TASK_3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TEST_TASK_3.getId(), task.getId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.findAll(""));
        final List<TaskDTO> tasks = SERVICE.findAll(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_TASK_ID));
        Assert.assertFalse(SERVICE.existsById(USER_ID, ""));
        Assert.assertFalse(SERVICE.existsById(USER_ID, NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(USER_ID, TEST_TASK_1.getId()));
    }

    @Test
    public void findByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", TEST_TASK_1.getId()));
        Assert.assertNull(SERVICE.findById(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = SERVICE.findById(USER_ID, TEST_TASK_1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TEST_TASK_1.getId(), task.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.clear(""));
        SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SERVICE.getSize(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(USER_ID, TEST_TASK_2);
        Assert.assertNull(SERVICE.findById(USER_ID, TEST_TASK_2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(USER_ID, ""));
        SERVICE.removeById(USER_ID, TEST_TASK_2.getId());
        Assert.assertNull(SERVICE.findById(USER_ID, TEST_TASK_2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.getSize(""));
        Assert.assertEquals(2, SERVICE.getSize(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create(null, TEST_TASK_3.getName(), TEST_TASK_3.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create("", TEST_TASK_3.getName(), TEST_TASK_3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null, TEST_TASK_3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, "", TEST_TASK_3.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, TEST_TASK_3.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, TEST_TASK_3.getName(), ""));
        @NotNull final TaskDTO task = SERVICE.create(USER_ID, TEST_TASK_3.getName(), TEST_TASK_3.getDescription());
        Assert.assertNotNull(task);
        @Nullable final TaskDTO findTask = SERVICE.findById(USER_ID, task.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(task.getId(), findTask.getId());
        Assert.assertEquals(TEST_TASK_3.getName(), task.getName());
        Assert.assertEquals(TEST_TASK_3.getDescription(), task.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateById(null, TEST_TASK_1.getId(), TEST_TASK_1.getName(), TEST_TASK_1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateById("", TEST_TASK_1.getId(), TEST_TASK_1.getName(), TEST_TASK_1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateById(USER_ID, null, TEST_TASK_1.getName(), TEST_TASK_1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateById(USER_ID, "", TEST_TASK_1.getName(), TEST_TASK_1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_TASK_1.getId(), null, TEST_TASK_1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_TASK_1.getId(), "", TEST_TASK_1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_TASK_1.getId(), TEST_TASK_1.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_TASK_1.getId(), TEST_TASK_1.getName(), ""));
        Assert.assertThrows(TaskNotFoundException.class, () -> SERVICE.updateById(USER_ID, NON_EXISTING_TASK_ID, TEST_TASK_1.getName(), TEST_TASK_1.getDescription()));
        @NotNull final String name = TEST_TASK_1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = TEST_TASK_1.getDescription() + NON_EXISTING_TASK_ID;
        SERVICE.updateById(USER_ID, TEST_TASK_1.getId(), name, description);
        @Nullable final TaskDTO task = SERVICE.findById(USER_ID, TEST_TASK_1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeTaskStatusById(null, TEST_TASK_1.getId(), status));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeTaskStatusById("", TEST_TASK_1.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> SERVICE.changeTaskStatusById(USER_ID, TEST_TASK_1.getId(), null));
        Assert.assertThrows(TaskNotFoundException.class, () -> SERVICE.changeTaskStatusById(USER_ID, NON_EXISTING_TASK_ID, status));
        SERVICE.changeTaskStatusById(USER_ID, TEST_TASK_1.getId(), status);
        @Nullable final TaskDTO task = SERVICE.findById(USER_ID, TEST_TASK_1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId(null, TEST_TASK_1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId("", TEST_TASK_1.getId());
        });
        @NotNull final Collection<TaskDTO> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_ID, null));
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_ID, ""));
        final List<TaskDTO> tasks = SERVICE.findAllByProjectId(USER_ID, TEST_PROJECT_1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
        tasks.forEach(task -> Assert.assertEquals(TEST_PROJECT_1.getId(), task.getProjectId()));
    }

}
