package ru.t1.bugakov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.marker.DatabaseCategory;
import ru.t1.bugakov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class ProjectSchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("liquibase/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("project");
    }
}
