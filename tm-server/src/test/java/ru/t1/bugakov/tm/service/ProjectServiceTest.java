package ru.t1.bugakov.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.api.service.dto.ITaskDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.marker.DatabaseCategory;
import ru.t1.bugakov.tm.marker.UnitCategory;
import ru.t1.bugakov.tm.migration.AbstractSchemeTest;
import ru.t1.bugakov.tm.service.dto.ProjectDTOService;
import ru.t1.bugakov.tm.service.dto.TaskDTOService;
import ru.t1.bugakov.tm.service.dto.UserDTOService;

import java.util.List;
import java.util.UUID;

@Category(DatabaseCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final String USER_TEST_LOGIN = "postgres";

    @NotNull
    private static final String USER_TEST_PASSWORD = "0000";

    private static IPropertyService PROPERTY_SERVICE;

    private static IConnectionService CONNECTION_SERVICE;

    private static IProjectDTOService SERVICE;

    private static ITaskDTOService TASK_SERVICE;

    private static IUserDTOService USER_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static final String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    private static final ProjectDTO TEST_PROJECT_1 = new ProjectDTO();

    @NotNull
    private static final ProjectDTO TEST_PROJECT_2 = new ProjectDTO();

    @NotNull
    private static final ProjectDTO TEST_PROJECT_3 = new ProjectDTO();

    @BeforeClass
    public static void setUp() throws Exception {
        //USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        //@Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        //USER_ID = user.getId();

        final Liquibase liquibase = liquibase("liquibase/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("project");

        PROPERTY_SERVICE = new PropertyService();
        CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);
        SERVICE = new ProjectDTOService(CONNECTION_SERVICE);
        TASK_SERVICE = new TaskDTOService(CONNECTION_SERVICE);
        USER_SERVICE = new UserDTOService(CONNECTION_SERVICE, SERVICE, TASK_SERVICE, PROPERTY_SERVICE);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        CONNECTION_SERVICE.close();
    }

    @Before
    public void before() throws Exception {
        TEST_PROJECT_1.setName("Test Project 1");
        TEST_PROJECT_1.setDescription("Test Description 1");
        TEST_PROJECT_2.setName("Test Project 2");
        TEST_PROJECT_2.setDescription("Test Description 2");
        TEST_PROJECT_3.setName("Test Project 3");
        TEST_PROJECT_3.setDescription("Test Description 3");
        SERVICE.add(USER_ID, TEST_PROJECT_1);
        SERVICE.add(USER_ID, TEST_PROJECT_2);
    }

    @After
    public void after() {
        SERVICE.clear(USER_ID);
    }

    @Test
    public void addByUserId() {
        @NotNull final ProjectDTO TEST_PROJECT_3 = new ProjectDTO();
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.add(null, TEST_PROJECT_3));
        @Nullable final ProjectDTO project = SERVICE.findById(USER_ID, TEST_PROJECT_3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(TEST_PROJECT_3.getId(), project.getId());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.findAll(""));
        final List<ProjectDTO> projects = SERVICE.findAll(USER_ID);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(USER_ID, project.getUserId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById(null, NON_EXISTING_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_PROJECT_ID));
        Assert.assertFalse(SERVICE.existsById(USER_ID, ""));
        Assert.assertFalse(SERVICE.existsById(USER_ID, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(USER_ID, TEST_PROJECT_1.getId()));
    }

    @Test
    public void findByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", TEST_PROJECT_1.getId()));
        Assert.assertNull(SERVICE.findById(USER_ID, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = SERVICE.findById(USER_ID, TEST_PROJECT_1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(TEST_PROJECT_1.getId(), project.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.clear(""));
        SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SERVICE.getSize(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(USER_ID, TEST_PROJECT_2);
        Assert.assertNull(SERVICE.findById(USER_ID, TEST_PROJECT_2.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(USER_ID, ""));
        SERVICE.removeById(USER_ID, TEST_PROJECT_2.getId());
        Assert.assertNull(SERVICE.findById(USER_ID, TEST_PROJECT_2.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.getSize(""));
        Assert.assertEquals(2, SERVICE.getSize(USER_ID));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create(null, TEST_PROJECT_3.getName(), TEST_PROJECT_3.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.create("", TEST_PROJECT_3.getName(), TEST_PROJECT_3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, null, TEST_PROJECT_3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.create(USER_ID, "", TEST_PROJECT_3.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, TEST_PROJECT_3.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.create(USER_ID, TEST_PROJECT_3.getName(), ""));
        @NotNull final ProjectDTO project = SERVICE.create(USER_ID, TEST_PROJECT_3.getName(), TEST_PROJECT_3.getDescription());
        Assert.assertNotNull(project);
        @Nullable final ProjectDTO findProject = SERVICE.findById(USER_ID, project.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(project.getId(), findProject.getId());
        Assert.assertEquals(TEST_PROJECT_3.getName(), project.getName());
        Assert.assertEquals(TEST_PROJECT_3.getDescription(), project.getDescription());
        Assert.assertEquals(USER_ID, project.getUserId());
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateById(null, TEST_PROJECT_1.getId(), TEST_PROJECT_1.getName(), TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.updateById("", TEST_PROJECT_1.getId(), TEST_PROJECT_1.getName(), TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateById(USER_ID, null, TEST_PROJECT_1.getName(), TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.updateById(USER_ID, "", TEST_PROJECT_1.getName(), TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_PROJECT_1.getId(), null, TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_PROJECT_1.getId(), "", TEST_PROJECT_1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_PROJECT_1.getId(), TEST_PROJECT_1.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> SERVICE.updateById(USER_ID, TEST_PROJECT_1.getId(), TEST_PROJECT_1.getName(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.updateById(USER_ID, NON_EXISTING_PROJECT_ID, TEST_PROJECT_1.getName(), TEST_PROJECT_1.getDescription()));
        @NotNull final String name = TEST_PROJECT_1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = TEST_PROJECT_1.getDescription() + NON_EXISTING_PROJECT_ID;
        SERVICE.updateById(USER_ID, TEST_PROJECT_1.getId(), name, description);
        @Nullable final ProjectDTO project = SERVICE.findById(USER_ID, TEST_PROJECT_1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeProjectStatusById(null, TEST_PROJECT_1.getId(), status));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.changeProjectStatusById("", TEST_PROJECT_1.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> SERVICE.changeProjectStatusById(USER_ID, TEST_PROJECT_1.getId(), null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.changeProjectStatusById(USER_ID, NON_EXISTING_PROJECT_ID, status));
        SERVICE.changeProjectStatusById(USER_ID, TEST_PROJECT_1.getId(), status);
        @Nullable final ProjectDTO project = SERVICE.findById(USER_ID, TEST_PROJECT_1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }


}
