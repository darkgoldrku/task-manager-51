package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.api.service.dto.ITaskDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.exception.entity.UserNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.LoginEmptyException;
import ru.t1.bugakov.tm.exception.field.PasswordEmptyException;
import ru.t1.bugakov.tm.exception.user.ExistsLoginException;
import ru.t1.bugakov.tm.marker.DatabaseCategory;
import ru.t1.bugakov.tm.marker.UnitCategory;
import ru.t1.bugakov.tm.service.dto.ProjectDTOService;
import ru.t1.bugakov.tm.service.dto.TaskDTOService;
import ru.t1.bugakov.tm.service.dto.UserDTOService;
import ru.t1.bugakov.tm.util.HashUtil;

import java.util.UUID;

@Category(DatabaseCategory.class)
public final class UserServiceTest {

    @NotNull
    private static final String USER_TEST_LOGIN = "Test admin";

    @NotNull
    private static final String USER_TEST_PASSWORD = "Test admin";

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(CONNECTION_SERVICE);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(CONNECTION_SERVICE);

    @NotNull
    private final IUserDTOService service = new UserDTOService(CONNECTION_SERVICE, projectService, taskService, PROPERTY_SERVICE);

    @NotNull
    private static UserDTO TEST_USER;

    @NotNull
    private static final String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @AfterClass
    public static void tearDown() {
        CONNECTION_SERVICE.close();
    }

    @Before
    public void before() {
        TEST_USER = new UserDTO("admin", HashUtil.salt(PROPERTY_SERVICE, "admin"), "admin@admin");
        service.add(TEST_USER);
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = service.findByLogin(USER_TEST_LOGIN);
        if (user != null) service.remove(user);
    }

    @Test
    public void add() {
        @Nullable final UserDTO user = service.findById(TEST_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER.getId(), user.getId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(TEST_USER.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(null));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(""));
        Assert.assertNull(service.findById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = service.findById(TEST_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER.getId(), user.getId());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(""));
        service.add(TEST_USER);
        Assert.assertNotNull(service.findById(TEST_USER.getId()));
        service.removeById(TEST_USER.getId());
        Assert.assertNull(service.findById(TEST_USER.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(null, USER_TEST_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create("", USER_TEST_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_TEST_LOGIN, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_TEST_LOGIN, ""));
        service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        @Nullable final UserDTO user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        @Nullable final UserDTO findUser = service.findById(user.getId());
        Assert.assertNotNull(findUser);
        Assert.assertEquals(user.getId(), findUser.getId());
        Assert.assertEquals(USER_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(TEST_USER.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(""));
        @Nullable final UserDTO user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER.getId(), user.getId());
    }

    @Test
    public void remove() throws Exception {
        service.add(TEST_USER);
        Assert.assertNotNull(service.findById(TEST_USER.getId()));
        service.remove(TEST_USER);
        Assert.assertNull(service.findById(TEST_USER.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByLogin(NON_EXISTING_USER_ID));
        service.add(TEST_USER);
        service.removeByLogin(TEST_USER.getLogin());
        Assert.assertNull(service.findById(TEST_USER.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(null, USER_TEST_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword("", USER_TEST_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(TEST_USER.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(TEST_USER.getId(), ""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.setPassword(NON_EXISTING_USER_ID, USER_TEST_PASSWORD));
        service.setPassword(TEST_USER.getId(), USER_TEST_PASSWORD);
        @Nullable final UserDTO user = service.findById(TEST_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER.getPasswordHash(), user.getPasswordHash());
        service.setPassword(TEST_USER.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser(null, firstName, lastName, middleName));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser("", firstName, lastName, middleName));
        service.updateUser(TEST_USER.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO user = service.findById(TEST_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(service.isLoginExist(null));
        Assert.assertFalse(service.isLoginExist(""));
        Assert.assertTrue(service.isLoginExist(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(service.isEmailExist(null));
        Assert.assertFalse(service.isEmailExist(""));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(NON_EXISTING_USER_ID));
        service.lockUserByLogin(USER_TEST_LOGIN);
        @Nullable final UserDTO user = service.findById(TEST_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> service.unlockUserByLogin(NON_EXISTING_USER_ID));
        service.lockUserByLogin(USER_TEST_LOGIN);
        service.unlockUserByLogin(USER_TEST_LOGIN);
        @Nullable final UserDTO user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
    }

}