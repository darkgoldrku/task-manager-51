package ru.t1.bugakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.bugakov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractDTORepository<E extends AbstractModelDTO> implements IAbstractDTORepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Override
    public void removeById(@NotNull String id) {
        @Nullable final E entity = findById(id);
        if (entity == null) return;
        remove(entity);
    }

    @Override
    public abstract void clear();

    @Nullable
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract List<E> findAllWithSort(@Nullable final String sortField);

    @Nullable
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract int getSize();

}

