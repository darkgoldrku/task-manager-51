package ru.t1.bugakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.bugakov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();

    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();

    }

    @Override
    public @Nullable List<TaskDTO> findAllWithSort(@NotNull String userId, @Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<TaskDTO> criteriaQuery = criteriaBuilder.createQuery(TaskDTO.class);
        @NotNull final Root<TaskDTO> root = criteriaQuery.from(TaskDTO.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public TaskDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);

    }

    @Override
    public int getSize(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();

    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM TaskDTO m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();

    }

    @Override
    public @Nullable List<TaskDTO> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<TaskDTO> criteriaQuery = criteriaBuilder.createQuery(TaskDTO.class);
        @NotNull final Root<TaskDTO> from = criteriaQuery.from(TaskDTO.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public @Nullable TaskDTO findById(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();

    }

}


