package ru.t1.bugakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDTOService extends IAbstractUserOwnedDTOService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    @NotNull
    TaskDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

}
