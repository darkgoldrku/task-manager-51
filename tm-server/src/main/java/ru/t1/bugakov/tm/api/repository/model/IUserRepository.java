package ru.t1.bugakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    boolean isLoginExists(@NotNull String login);

    boolean isEmailExists(@NotNull String email);

}
