package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Login user.";
    }

}
