package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataYamlLoadFasterXmlRequest;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        getDomainEndpoint().YamlLoadFasterXml(new DataYamlLoadFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-load-yaml-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from yaml file with fasterxml";
    }

}
