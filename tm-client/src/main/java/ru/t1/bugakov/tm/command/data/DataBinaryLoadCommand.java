package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        getDomainEndpoint().BinaryLoad(new DataBinaryLoadRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return NAME;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from binary file";
    }

}
